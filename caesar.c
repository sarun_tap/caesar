#include <stdio.h>
#include <stdlib.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

//Prototype
void print_cipher(string plainText, int key);

int main(int argc, string argv[])
{
    if (argc != 2)
    {
        printf("Usage: ./caesar key\n");
        return 1;
    }

    //Check if the input is decimal
    for (int i = 0, n = strlen(argv[1]); i < n; i++)
    {
        char c = argv[1][i];
        if (!isalnum(c))
        {
            printf("Usage: ./caesar key\n");
            return 1;
        }
    }

    int key = atoi(argv[1]);
    string plainText = get_string("plaintext: ");
    print_cipher(plainText, key);

}

void print_cipher(string plainText, int key)
{
    printf("ciphertext: ");
    for (int i = 0, n = strlen(plainText); i < n; i ++)
    {
        char c = plainText[i];
        char cipherChar;
        if (c >= 'A' && c <= 'Z')
        {
            cipherChar = (c + key - 'A') % 26 + 'A';
        }
        else if (c >= 'a' && c <= 'z')
        {
            cipherChar = (c + key - 'a') % 26 + 'a';
        }
        else
        {
            cipherChar = c;
        }
        printf("%c", cipherChar);
    }
    printf("\n");
}